blog
****

Django app for a simple blog:
https://www.kbsoftware.co.uk/docs/app-blog.html

Install
=======

Virtual Environment
-------------------

If you've got dev-scripts installed type::

  create-venv blog

Otherwise::

  virtualenv --python=python3 venv-blog
  # or
  python3 -m venv venv-blog

  source venv-blog/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  py.test -x

Usage
=====

To set-up the test database and load location data::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
