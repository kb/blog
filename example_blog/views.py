# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.views.generic import TemplateView


class SettingsView(LoginRequiredMixin, StaffuserRequiredMixin, TemplateView):
    template_name = "example/settings.html"
