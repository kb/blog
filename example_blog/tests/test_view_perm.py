# -*- encoding: utf-8 -*-
import pytest

from dateutil.relativedelta import relativedelta
from http import HTTPStatus

from django.urls import reverse
from django.utils import timezone

from block.tests.factories import PageFactory, TemplateFactory
from block.models import Page
from blog.models import BlogPost
from blog.tests.factories import BlogPostFactory
from blog.tests.test_view_perm import _create_page
from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_home(client, perm_check):
    """Test that home in example app redirects to blog home."""
    _create_page(BlogPost.PAGE_BLOG_HOME_SLUG, "")
    BlogPostFactory(date_published=timezone.now() + relativedelta(days=-2))
    BlogPostFactory(date_published=timezone.now() + relativedelta(days=-1))
    BlogPostFactory(date_published=timezone.now())
    redirect_response = client.get(reverse("project.home"))
    assert redirect_response.status_code == HTTPStatus.FOUND
    perm_check.anon(redirect_response.url)


@pytest.mark.django_db
def test_settings(perm_check):
    url = reverse("project.settings")
    perm_check.staff(url)
