# -*- encoding: utf-8 -*-
import pytest

from django.utils import timezone
from django.urls import reverse
from http import HTTPStatus

from block.tests.factories import PageFactory, TemplateFactory
from blog.models import BlogPost
from blog.tests.factories import BlogPostFactory
from login.tests.factories import TEST_PASSWORD, UserFactory


@pytest.mark.django_db
def test_home(client):
    PageFactory(
        slug=BlogPost.PAGE_BLOG_HOME_SLUG,
        slug_menu="",
        is_custom=False,
        template=TemplateFactory(template_name="example/settings.html"),
    )
    post = BlogPostFactory(date_published=timezone.now(), title="First Post")
    redirect_response = client.get(reverse("project.home"))
    assert redirect_response.status_code == HTTPStatus.FOUND
    response = client.get(redirect_response.url)
    assert response.status_code == HTTPStatus.OK
    list = response.context["object_list"]
    assert response.status_code == HTTPStatus.OK
    assert post in list


def _login_stan(client):
    """User is logged in"""
    u = UserFactory(
        username="staff",
        first_name="Stan",
        last_name="Stafford",
        email="stan@example.com",
        is_staff=True,
    )
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    return u


@pytest.mark.django_db
def test_delete(client):
    _login_stan(client)
    PageFactory(
        slug=BlogPost.PAGE_BLOG_HOME_SLUG,
        slug_menu="",
        is_custom=False,
        template=TemplateFactory(template_name="example/settings.html"),
    )
    BlogPostFactory(date_published=timezone.now(), title="a")

    post_b = BlogPostFactory(date_published=timezone.now(), title="b")
    BlogPostFactory(date_published=timezone.now(), title="c")
    redirect_response = client.get(reverse("project.home"))
    assert redirect_response.status_code == HTTPStatus.FOUND
    response = client.get(redirect_response.url)
    assert HTTPStatus.OK == response.status_code
    assert "object_list" in response.context
    # order by published
    assert ["c", "b", "a"] == [x.title for x in response.context["object_list"]]
    response = client.post(reverse("blog.delete", args=[post_b.pk]), {})
    assert HTTPStatus.FOUND == response.status_code

    redirect_response = client.get(reverse("project.home"))
    assert redirect_response.status_code == HTTPStatus.FOUND
    response = client.get(redirect_response.url)
    assert HTTPStatus.OK == response.status_code
    assert "object_list" in response.context
    # b not displayed as marked deleted
    assert ["c", "a"] == [x.title for x in response.context["object_list"]]
