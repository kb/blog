# -*- encoding: utf-8 -*-
from django.conf import settings

# from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include, reverse_lazy
from django.views.generic import RedirectView

from example_blog.views import SettingsView


admin.autodiscover()


urlpatterns = [
    path("admin/", admin.site.urls),
    # no home page redirect to the blog home page
    path(
        "",
        RedirectView.as_view(url=reverse_lazy("blog.home")),
        name="project.home",
    ),
    path("settings/", SettingsView.as_view(), name="project.settings"),
    path("", include("login.urls")),
    path("blog/", view=include("blog.urls")),
    path("gallery/", include("gallery.urls")),
    path("gdpr/", include("gdpr.urls")),
    path("wizard/", include("block.urls.wizard")),
    path("block/", include("block.urls.block")),
    path(
        "dash/",
        RedirectView.as_view(url=reverse_lazy("blog.list")),
        name="project.dash",
    ),
    path("", include("block.urls.cms")),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#   ^ helper function to return a URL pattern for serving files in debug mode.
# https://docs.djangoproject.com/en/1.5/howto/static-files/#serving-files-uploaded-by-a-user

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
