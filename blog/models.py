# -*- encoding: utf-8 -*-
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django_extensions.db.fields import AutoSlugField
from reversion import revisions as reversion

from base.model_utils import TimedCreateModifyDeleteModel
from gallery.models import Image, Wizard


class BlogRole(TimedCreateModifyDeleteModel):
    CONTRIBUTOR = "contributor"
    PUBLISHER = "publisher"
    BOTH = "contributor-publisher"

    slug = models.SlugField(unique=True)
    title = models.CharField(max_length=200)

    class Meta:
        ordering = ("slug",)
        verbose_name = "Blog Role"
        verbose_name_plural = "Blog Roles"

    def __str__(self):
        return "{}".format(self.title)


reversion.register(BlogRole)


class BlogUser(TimedCreateModifyDeleteModel):
    role = models.ForeignKey(BlogRole, on_delete=models.CASCADE)
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        ordering = ("user",)
        verbose_name = "Blog User"
        verbose_name_plural = "Blog Users"

    def __str__(self):
        if self.user.first_name or self.user.last_name:
            str = (
                self.user.first_name
                + (" " if self.user.first_name and self.user.last_name else "")
                + self.user.last_name
            )
        else:
            str = self.user.username
        return str


reversion.register(BlogUser)


class BlogPostManager(models.Manager):
    def current(self):
        return self.model.objects.exclude(deleted=True)

    def published(self):
        return (
            self.model.objects.current()
            .filter(date_published__lt=timezone.now())
            .exclude(date_published__isnull=True)
        )


class WizardModelMixin:
    """Wizard Model Mixin - should probably be in the block app."""

    @property
    def wizard_urls(self):
        """Return the URLs for the image and link wizards."""
        result = []
        if hasattr(self, "wizard_fields"):
            for item in self.wizard_fields:
                content_type = ContentType.objects.get_for_model(self)
                result.append(
                    {
                        "caption": item.field_name.title(),
                        "class": item.css_class,
                        "url": reverse(
                            item.url_name,
                            kwargs={
                                "content": content_type.pk,
                                "pk": self.pk,
                                "field": item.field_name,
                                "type": item.link_type,
                            },
                        ),
                    }
                )
        return result


class BlogPost(TimedCreateModifyDeleteModel, WizardModelMixin):
    PAGE_BLOG_HOME_SLUG = "blog"
    PAGE_BLOG_POST_MENU = "blog-post"
    PAGE_BLOG_PREVIEW_MENU = "blog-preview"

    slug = AutoSlugField(populate_from=["title"])
    title = models.CharField(max_length=100)
    description = models.TextField()
    author = models.ForeignKey(
        BlogUser, blank=True, null=True, on_delete=models.CASCADE
    )
    picture = models.ForeignKey(
        Image,
        related_name="blogpost_picture",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )

    date_published = models.DateTimeField(blank=True, null=True)
    objects = BlogPostManager()

    class Meta:
        ordering = ["-date_published", "-created"]
        verbose_name = "Blog Post"
        verbose_name_plural = "Blog Posts"

    def __str__(self):
        return "{}: {}, {}".format(
            self.title, self.author, self.date_published or "Not Published"
        )

    # properties and methods for the image wizard
    def get_design_url(self):
        return reverse("blog.preview", args=[self.pk])

    def get_absolute_url(self):
        return reverse("blog.post", kwargs={"slug": self.slug})

    def set_pending_edit(self):
        return True

    @property
    def wizard_fields(self):
        return [Wizard("picture", Wizard.IMAGE, Wizard.SINGLE)]


reversion.register(BlogPost)
