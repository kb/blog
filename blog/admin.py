# -*- encoding: utf-8 -*-
from django.contrib import admin

from .models import BlogRole, BlogUser


admin.site.register(BlogRole)
admin.site.register(BlogUser)
