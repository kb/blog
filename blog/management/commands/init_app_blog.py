# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from block.models import Page, Section, Template, TemplateSection

from blog.models import BlogPost


class Command(BaseCommand):
    help = "Initialise 'blog' application"

    def handle(self, *args, **options):
        self.stdout.write("Create blog pages...")

        """
        header = Section.objects.init_section(
            slug="header_a",
            name="Top Header",
            block_app="compose",
            block_model="Header",
            create_url_name="compose.header.create",
        )
        """

        pages = [
            {
                "slug": BlogPost.PAGE_BLOG_HOME_SLUG,
                "slug_menu": "",
                "template": "blog/blog_home.html",
                "name": "Blog Home",
                "sections": [],  # [header],
            },
            {
                "slug": Page.CUSTOM,
                "slug_menu": BlogPost.PAGE_BLOG_POST_MENU,
                "template": "blog/blog_post.html",
                "name": "Blog Post",
                "sections": [],
            },
            {
                "slug": Page.CUSTOM,
                "slug_menu": BlogPost.PAGE_BLOG_PREVIEW_MENU,
                "template": "blog/blog_preview.html",
                "name": "Blog Preview",
                "sections": [],
            },
        ]
        for info in pages:
            template = Template.objects.init_template(
                info["slug"], info["template"]
            )

            for section in info["sections"]:
                TemplateSection.objects.init_template_section(
                    template=template, section=section
                )

            Page.objects.init_page(
                slug_page=info["slug"],
                slug_menu=info["slug_menu"],
                name=info["name"],
                order=0,
                template=template,
                is_custom=info["slug"] == Page.CUSTOM,
            )

            Page.objects.refresh_sections_from_template(template)

        self.stdout.write("Initialised 'blog' app")
