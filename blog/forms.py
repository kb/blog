# -*- encoding: utf-8 -*-
import logging

from django import forms

from base.form_utils import RequiredFieldForm
from .models import BlogPost


logger = logging.getLogger(__name__)


class BlogPostEmptyForm(forms.ModelForm):
    class Meta:
        model = BlogPost
        fields = ()


class BlogPostForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        self.req = kwargs.pop("request")
        super().__init__(*args, **kwargs)
        for name in ("title", "description", "author"):
            self.fields[name].widget.attrs.update(
                {"class": "pure-input-1", "rows": 4}
            )
        self.fields["description"].help_text = "Please enter your message"

    class Meta:
        model = BlogPost
        fields = ("title", "description", "author")
