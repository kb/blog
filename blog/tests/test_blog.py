# -*- encoding: utf-8 -*-
import pytest

from blog.models import BlogRole
from blog.tests.factories import (
    BlogPostFactory,
    BlogRoleFactory,
    BlogUserFactory,
)
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_blogpost_str():
    author = BlogUserFactory()
    post = BlogPostFactory(
        title="First Post", author=author, description="A First Post"
    )
    assert "First Post: {}, Not Published".format(author) == str(post)


@pytest.mark.django_db
def test_blogrole_str():
    role = BlogRoleFactory(slug=BlogRole.PUBLISHER, title="Publisher")
    assert "Publisher" == str(role)


@pytest.mark.django_db
def test_bloguser_str_username():
    user = BlogUserFactory(
        user=UserFactory(first_name="", last_name="", username="jo-lo")
    )
    assert "jo-lo" == str(user)


@pytest.mark.django_db
def test_bloguser_str():
    user = BlogUserFactory(user=UserFactory(first_name="Jo", last_name="Lee"))
    assert "Jo Lee" == str(user)
