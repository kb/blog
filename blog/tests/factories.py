# -*- encoding: utf-8 -*-
import factory

from blog.models import BlogPost, BlogRole, BlogUser
from login.tests.factories import UserFactory


class BlogRoleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = BlogRole

    @factory.sequence
    def title(n):
        return "title_{:02d}".format(n)

    @factory.sequence
    def slug(n):
        return "slug_{:02d}".format(n)


class BlogUserFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    role = factory.SubFactory(BlogRoleFactory)

    class Meta:
        model = BlogUser


class BlogPostFactory(factory.django.DjangoModelFactory):
    author = factory.SubFactory(BlogUserFactory)

    class Meta:
        model = BlogPost

    @factory.sequence
    def title(n):
        return "title_{:02d}".format(n)

    @factory.sequence
    def description(n):
        return "desciption_{:02d}".format(n)
