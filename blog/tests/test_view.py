# -*- encoding: utf-8 -*-
import io
import PIL
import pytest

from django.contrib.contenttypes.models import ContentType
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
from http import HTTPStatus

from blog.models import BlogPost, BlogRole, BlogUser
from blog.tests.factories import BlogPostFactory, BlogUserFactory
from gallery.models import Image, Wizard
from gallery.tests.factories import ImageCategoryFactory, ImageFactory
from login.tests.factories import TEST_PASSWORD, UserFactory


def _login_stan(client):
    """User is logged in"""
    u = UserFactory(
        username="staff",
        first_name="Stan",
        last_name="Stafford",
        email="stan@example.com",
        is_staff=True,
    )
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    return u


@pytest.mark.django_db
def test_list(client):
    _login_stan(client)
    BlogPostFactory(title="a")
    BlogPostFactory(title="b")
    response = client.get(reverse("blog.list"))
    assert HTTPStatus.OK == response.status_code
    assert "blogpost_list" in response.context
    # order by created
    assert ["b", "a"] == [x.title for x in response.context["blogpost_list"]]


def _data(user):
    try:
        author = BlogUser.objects.get(user=user)
    except BlogUser.DoesNotExist:
        contributor, created = BlogRole.objects.get_or_create(
            slug=BlogRole.CONTRIBUTOR
        )
        author = BlogUserFactory(user=user, role=contributor)

    return {
        "description": "We like to write",
        "author": author,
        "title": "What we like",
    }


def _post(client, url, user):
    data = _data(user)
    data["author"] = data["author"].pk
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code
    return response


@pytest.mark.django_db
def test_create(client):
    """check blog."""
    user = _login_stan(client)
    response = _post(client, reverse("blog.create"), user)
    assert HTTPStatus.FOUND == response.status_code
    # check the post was created
    post = BlogPost.objects.get(
        author=BlogUser.objects.get(role__slug=BlogRole.CONTRIBUTOR)
    )
    assert post.title == "What we like"
    assert post.description == "We like to write"
    assert post.author.user == user
    assert not post.date_published


@pytest.mark.django_db
def test_delete(client):
    _login_stan(client)
    BlogPostFactory(title="a")
    post_b = BlogPostFactory(title="b")
    BlogPostFactory(title="c")
    response = client.get(reverse("blog.list"))
    assert HTTPStatus.OK == response.status_code
    assert "blogpost_list" in response.context
    # order by created
    assert ["c", "b", "a"] == [
        x.title for x in response.context["blogpost_list"]
    ]
    response = client.post(reverse("blog.delete", args=[post_b.pk]), {})
    assert HTTPStatus.FOUND == response.status_code

    response = client.get(reverse("blog.list"))
    assert HTTPStatus.OK == response.status_code
    assert "blogpost_list" in response.context
    assert ["c", "b", "a"] == [
        x.title for x in response.context["blogpost_list"]
    ]
    delete_flag_checked = False

    for x in response.context["blogpost_list"]:
        if x.title == "b":
            assert x.deleted
            delete_flag_checked = True

    assert delete_flag_checked


@pytest.mark.django_db
def test_publish(client):
    user = _login_stan(client)
    post = BlogPostFactory(**_data(user))
    assert not post.date_published
    response = client.post(reverse("blog.publish", args=[post.pk]), {})
    assert HTTPStatus.FOUND == response.status_code
    # initial
    post.refresh_from_db()
    assert post.date_published


@pytest.mark.django_db
def test_update_get(client):
    """User is logged in"""
    user = _login_stan(client)
    data = _data(user)
    post = BlogPostFactory(**data)
    response = client.get(reverse("blog.update", args=[post.pk]))
    # initial
    assert response.status_code == HTTPStatus.OK
    assert "form" in response.context
    form = response.context["form"]
    assert "author" in form.initial
    assert "title" in form.initial
    assert "description" in form.initial
    assert form.initial["author"] == data["author"].pk
    assert form.initial["description"] == data["description"]
    assert form.initial["title"] == data["title"]


@pytest.mark.django_db
def test_update_post(client):
    """User is logged in"""
    user = _login_stan(client)
    post = BlogPostFactory()
    data = _data(user)
    response = _post(client, reverse("blog.update", args=[post.pk]), user)
    assert response.status_code == HTTPStatus.FOUND
    # initial
    post.refresh_from_db()
    assert post.author == data["author"]
    assert post.description == data["description"]
    assert post.title == data["title"]


def test_file():
    """create an (image) file ready to upload."""
    fp = io.BytesIO()
    PIL.Image.new("1", (1, 1)).save(fp, "png")
    fp.seek(0)
    return SimpleUploadedFile("file.png", fp.read(), content_type="image/png")


def _reverse_wizard_url(content, url_name, field_name, wizard_type, category):
    """Get the wizard url for a piece of content."""
    content_type = ContentType.objects.get_for_model(content)
    kwargs = {
        "content": content_type.pk,
        "pk": content.pk,
        "field": field_name,
        "type": wizard_type,
    }
    if category:
        kwargs.update(category=category.slug)
    return reverse(url_name, kwargs=kwargs)


def url_image_single(content, url_name, category=None):
    return _reverse_wizard_url(
        content, url_name, "picture", Wizard.SINGLE, category
    )


@pytest.mark.django_db
def test_blogpost_wizard_image_upload(client):
    post = BlogPostFactory()
    category = ImageCategoryFactory()
    ImageFactory()
    # image = ImageFactory()
    user = UserFactory(is_staff=True)
    assert post.picture is None
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = url_image_single(post, "gallery.wizard.image.upload")
    data = {
        "add_to_library": True,
        "category": category.pk,
        "image": test_file(),
        "title": "Cricket",
    }
    response = client.post(url, data)
    # check
    post.refresh_from_db()
    expect = post.get_design_url()
    assert 302 == response.status_code
    assert expect == response["Location"]
    assert "Cricket" == post.picture.title
    assert post.picture is not None
    assert post.picture.category == category
    assert post.picture.deleted is False
    # check an image has been added to the database
    assert 2 == Image.objects.count()
