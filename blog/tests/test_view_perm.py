# -*- encoding: utf-8 -*-
import pytest

from dateutil.relativedelta import relativedelta
from django.urls import reverse
from django.utils import timezone

from block.tests.factories import PageFactory, TemplateFactory
from block.models import Page
from blog.models import BlogPost
from blog.tests.factories import BlogPostFactory, BlogUserFactory
from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_list(perm_check):
    BlogPostFactory()
    BlogPostFactory()
    url = reverse("blog.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_create(perm_check):
    url = reverse("blog.create")
    perm_check.staff(url)


@pytest.mark.django_db
def test_delete(perm_check):
    post = BlogPostFactory()
    url = reverse("blog.delete", args=[post.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_update(perm_check):
    post = BlogPostFactory()
    url = reverse("blog.update", args=[post.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_publish(perm_check):
    post = BlogPostFactory()
    url = reverse("blog.publish", args=[post.pk])
    perm_check.staff(url)


def _create_page(slug, menu):
    PageFactory(
        slug=slug,
        slug_menu=menu,
        is_custom=slug == Page.CUSTOM,
        template=TemplateFactory(template_name="blog/blogpost_list.html"),
    )


@pytest.mark.django_db
def test_blog_home(perm_check):
    _create_page(BlogPost.PAGE_BLOG_HOME_SLUG, "")
    BlogPostFactory(date_published=timezone.now() + relativedelta(days=-2))
    BlogPostFactory(date_published=timezone.now() + relativedelta(days=-1))
    BlogPostFactory(date_published=timezone.now())
    url = reverse("blog.home")
    perm_check.anon(url)


@pytest.mark.django_db
def test_blog_post(perm_check):
    _create_page(Page.CUSTOM, BlogPost.PAGE_BLOG_POST_MENU)
    post = BlogPostFactory(date_published=timezone.now())
    url = reverse("blog.post", args=[post.slug])
    perm_check.anon(url)


@pytest.mark.django_db
def test_blog_preview(perm_check):
    _create_page(Page.CUSTOM, BlogPost.PAGE_BLOG_PREVIEW_MENU)
    post = BlogPostFactory(date_published=timezone.now())
    url = reverse("blog.preview", args=[post.pk])
    perm_check.staff(url)
