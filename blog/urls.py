# -*- encoding: utf-8 -*-
from django.urls import path

from block.models import Page
from block.views import CmsPageDesignView

from .views import (
    BlogPostCreateView,
    BlogPostDeleteView,
    BlogPostMaintenanceListView,
    BlogPostPreviewView,
    BlogPostPublishedListView,
    BlogPostPublishView,
    BlogPostUpdateView,
    BlogPostView,
)


urlpatterns = [
    # design rule is required so it's not hidden by "" rule below
    path(
        "design/",
        CmsPageDesignView.as_view(),
        kwargs={"page": "blog"},
        name="project.page.design",
    ),
    path("list/", BlogPostMaintenanceListView.as_view(), name="blog.list"),
    path("add/", BlogPostCreateView.as_view(), name="blog.create"),
    path("<int:pk>/delete/", BlogPostDeleteView.as_view(), name="blog.delete"),
    path(
        "<int:pk>/preview/",
        BlogPostPreviewView.as_view(),
        kwargs={"page": Page.CUSTOM, "menu": "blog-preview"},
        name="blog.preview",
    ),
    path(
        "<int:pk>/publish/", BlogPostPublishView.as_view(), name="blog.publish"
    ),
    path("<int:pk>/update/", BlogPostUpdateView.as_view(), name="blog.update"),
    path(
        "<slug:slug>/",
        BlogPostView.as_view(),
        kwargs={"page": Page.CUSTOM, "menu": "blog-post"},
        name="blog.post",
    ),
    path(
        "",
        BlogPostPublishedListView.as_view(),
        kwargs={"page": "blog", "menu": ""},
        name="blog.home",
    ),
]
