# -*- encoding: utf-8 -*-
from django.views.generic import CreateView, ListView, UpdateView

from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.contrib import messages
from django.db import transaction
from django.utils import timezone
from django.http import HttpResponseRedirect
from django.urls import reverse

from base.view_utils import BaseMixin
from block.views import PageDetailView, PageFormMixin
from .forms import BlogPostEmptyForm, BlogPostForm
from .models import BlogPost


class BlogPostCreateMixin:
    form_class = BlogPostForm
    model = BlogPost

    def form_valid(self, form):
        """Do the form save within a transaction."""
        with transaction.atomic():
            result = super().form_valid(form)
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(dict(request=self.request))
        return kwargs


class BlogPostCreateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    BlogPostCreateMixin,
    BaseMixin,
    CreateView,
):
    """Save a blog in the database."""

    def get_success_url(self):
        return reverse("blog.list")


class BlogPostDeleteView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    """Approve application."""

    form_class = BlogPostEmptyForm
    model = BlogPost
    template_name = "blog/blogpost_delete.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_deleted(self.request.user)
        messages.info(
            self.request,
            "Removed the '{}' Blog post".format(self.object.title),
        )
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("blog.list")


class BlogPostMaintenanceListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    model = BlogPost
    paginate_by = 10


class BlogPostUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = BlogPostForm
    model = BlogPost

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(dict(request=self.request))
        return kwargs

    def get_success_url(self):
        return reverse("blog.list")


class BlogPostPublishView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    template_name = "blog/blogpost_publish.html"
    form_class = BlogPostEmptyForm
    model = BlogPost

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.date_published = timezone.now()
            self.object.save()
            messages.info(
                self.request, "Published {}".format(self.object.title)
            )
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("blog.list")


class BlogPostPreviewView(
    LoginRequiredMixin, StaffuserRequiredMixin, PageDetailView
):

    """Display a preview of the blog post.
    e.g. before it is published
    Expects a page to be defined with a slug of Page.CUSTOM and slug_menu
    of "blog-preview" (see entry in blog/urls.py)
    """

    model = BlogPost


class BlogPostPublishedListView(PageFormMixin, ListView):

    """Display a list of published blog posts.
    Expects a page to be defined with a slug of Page.CUSTOM and slug_menu
    of "blog-home" (see entry in blog/urls.py)
    """

    model = BlogPost
    paginate_by = 10
    page_kwarg = "page-no"

    def get_queryset(self):
        return BlogPost.objects.published()


class BlogPostView(PageDetailView):

    """Display a blog post.
    Expects a page to be defined with a slug of Page.CUSTOM and slug_menu
    of "blog-post" (see entry in blog/urls.py)
    """

    model = BlogPost
    # template_name = "blog/blog_post.html"
